import './c.js'



/* 

    仅为副作用而导入一个模块
模块仅为副作用（中性词、无贬义含义）而导入，而不是导入模块中的任何内容，这将运行模块中的全局代码，但实际上不导入任何值。

import “/modules/my-module.js”


*/
