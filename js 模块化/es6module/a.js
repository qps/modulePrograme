//常规方式

// import {aacopy} from "./b.js";

// 在改个名字
// import {aacopy as aa} from "./b.js";

// console.log(aa)

// 默认导出的时候不能用 {} 了

// import aa1 from "./b.js"
// aa1()


// 复杂导出
// import * as obj from "./b.js";
// console.log('obj',obj);
// console.log('a',obj.a);
// obj.default()
// import {a} from "./b.js";
// console.log('a', {a});
