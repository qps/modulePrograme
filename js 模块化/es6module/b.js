
// 先声明后导出

// var obj={name:'qps'};
// export {obj}

// 边声明边导出

// export function fn (){
//     console.log(1)
// }

// 改名字

// let aa='pqs'
// export {aa as aacopy}


// 默认导出 在导入的时候不需要名字

export default function aa(){
    console.log("我是中国人")
}

// export var a=1
// export var b=1

// var a=1;
// export  {a}

