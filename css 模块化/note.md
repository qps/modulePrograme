# css module

> http://www.ruanyifeng.com/blog/2016/06/css_modules.html  来自阮一峰的 css module 用法教程

    - 学过网页开发就会知道，CSS 不能算编程语言，只是网页样式的一种描述方法。
      为了让 CSS 也能适用软件工程方法，程序员想了各种办法，让它变得像一门编程语言(拥有变量，循环，判断等))。从最早的Less、SASS，到后来的 PostCSS，再到最近的 CSS in JS，都是为了解决这个问题。

    - 本文介绍的 CSS Modules 有所不同。它不是将 CSS 改造成编程语言，而是功能很单纯，只加入了局部作用域和模块依赖，这恰恰是网页组件最急需的功能。