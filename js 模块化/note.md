# js 模块化规范
## 最早是闭包的写法，防止变量的污染  ,多个js 用 defer 和 async
    -  https://www.jianshu.com/p/3c4b371b0c35
    —  defer与async的区别是：defer要等到整个页面在内存中正常渲染结束（DOM 结构完全生成，以及其他脚本执行完成），才会执行；async一旦下载完，渲染引擎就会中断渲染，执行这个脚本以后，再继续渲染。一句话，defer是“渲染完再执行”，async是“下载完就执行”。而且多个js 文件的话defer 可以保证js文件的下载的顺序 。
## amd 和 cmd 在es6 module 出现之后就退出历史的舞台了
    amd 规范 典型的 应用，require.js
    require.js 用法  http://www.softwhy.com/article-7653-1.html

## es6 module 
>  浏览器加载 ES6 模块，也使用<script>标签，但是要加入type="module"属性
    type="module" 等同于打开了<script>标签的defer属性
    注意：必须得以服务器的模式打开   要不然会有跨域的错误   可以用 npm 安装一个anywhere
> dem> es6 module
    > 一个模块只有一个默认导出，但是可以是默认导出和对象导出的组合


    - export 语法

        -  Named exports 命名导出

            ```
                // 1)声明时导出
                    export var myVar1 = 'a';
                    export let myVar2 = 'b';
                    export const MY_CONST = 'c';
                    export function myFunc() {}
                    
                    // 2)声明后导出
                    var myVar3 = 'a';
                    export { myVar3 };
                    
                    // 3)别名导出
                    var myVar4 = 'a';
                    export { myVar4 as myVar };
            ```
        
        -   Default exports 默认导出

            ```
                // 1)声明时导出
                export default expression;
                export default function () {}
                
                // 2)别名设置为default导出
                export default function name1() {}
                export { name1 as default };

            ```

    - import 语法

        ```
            // 1)导入模块的默认导出内容（defaultExport 可以是任意名字） 不需要{}了
            import defaultExport from 'module-name';
            
            // 2)导入模块的命名导出内容
            import { export1, export2 } from 'module-name';
            import { export as alias } from 'module-name'; // 修改别名
            import * as name from 'module-name'; // 导入模块内的所有内容
            
            // 3)导入模块的默认导出、命名导出
            import defaultExport, { export1, export2 } from 'module-name';
            import defaultExport, * as name from 'module-name';


             //    仅为副作用而导入一个模块 模块仅为副作用（中性词、无贬义含义）而导入，而不是导入模块中的任何内容，这将运行模块中的全局代码，但实际上不导入任何值。
             import “/modules/my-module.js”
        
        ```